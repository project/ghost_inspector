## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Support

## INTRODUCTION

Ghost Inspector Module allows you to view Suite API integration from the Ghost Inspector website.
Daily sync and purge of the old tests are done via Cron Job as per your configuration setup.
A notification email of the last test run will be sent if the test run failed.
Ghost Inspector Content is set to Published = No. Reason: We don't want test results to show on the website. Administration use only.

Reporting also available:
 * Suite History Results
 * Suite Test Results
 * Dashboard
 * Failed vs Passed Report per Day
 * Failed vs Passed Report per Month

Font Awesome 5 Free is used in the module for icons display.

## REQUIREMENTS

This module requires the below modules outside of Drupal core:
 * Download the Paragraphs module via Composer: composer require drupal/paragraphs
 * Download the Better Exposed Filters module via Composer: composer require drupal/better_exposed_filters
 * Download the Twig Tweak module via Composer: composer require drupal/twig_tweak

Views, Filter, User, System, Paragraphs, Entity Reference Revisions, Field, File, Twig Tweak, Better Exposed Filters

## INSTALLATION

Download the Ghost Inspector module via Composer:
composer require drupal/ghost_inspector

## CONFIGURATION

1. Navigate to "Admin > Extend" and enable module.
2. Navigate to "Admin > Configuration > System > Ghost Inspector" to configure options.
   - Click on Add Suite API Setting
     - API Setting
     - Purge old Test Runs
     - Email Notification on last Synced Run
     - Save configuration to see what selections were done.
3. On a successful API call is done the Suite will be active.
4. Click on "Sync Action" to sync the suite Results from Ghost Inspector.
5. On sync completion, all content will be Published = No. *Reason: We don't want test results to show on the website. Administration use only.*
6. Click on the Suite Name to view test runs.
   - Views on:
     - Test runs
     - Test cases
     - Test steps
7. Reporting:
   - Full dashboard Report
   - Failed vs Passed Report per Day
   - Failed vs Passed Report per Month
8. Optional: Cron Job is enabled. Cron will run every 15 Min to check new test runs on the Suite.

## DOCUMENTATION

For detailed documentation please visit the link below:
https://www.drupal.org/docs/contributed-modules/ghost-inspector-integrator

## SUPPORT

For bug reports and feature requests please use the Drupal.org issue tracker:
http://drupal.org/project/issues/ghost_inspector

We welcome your support in improving code documentation, tests, and providing
example use-cases not addressed by the existing module.

