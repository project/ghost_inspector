<?php

namespace Drupal\ghost_inspector;

use DateTime;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Ghost Inspector setting entities.
 */
class GhostInspectorEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * The config factory that knows what is overwritten.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('config.factory')
    );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ConfigFactoryInterface $config_factory) {
    parent::__construct($entity_type, $storage);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Suite Name');
    $header['tests_to_keep'] = $this->t('Tests to keep');
    $header['sync_daily'] = $this->t('Sync Daily');
    $header['email_enabled'] = $this->t('Email Enabled');
    $header['test_runs'] = $this->t('Test Runs on CMS');
    $header['last_date_synced'] = $this->t('Last Date Synced');
    $header['test_statuses'] = $this->t('Last Test Statuses');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function buildRow(EntityInterface $entity) {
    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'ghost_inspector')
      ->condition('field_suite_id', $entity->id(), '=')
      ->execute();

    $row['name'] = $entity->get('name');
    if (strpos($entity->get('name'), "API Call Failed") !== FALSE) {
      $row['name'] = ['class' => "api-red", 'data' => $entity->get('name')];
    }
    elseif (count($nids) === 0) {
      $row['name'] = $entity->get('name') . ": Please Sync Test Suite.";
    }
    else {
      $options = ['absolute' => TRUE];
      $url = Url::fromRoute('ghost_inspector.dashboard', ['ghost_inspector' => $entity->get('id')], $options);
      $link = Link::fromTextAndUrl($entity->get('name'), $url)->toString();
      $row['name'] = $link;
    }
    if ($entity->get('tests_to_keep') > 1) {
      $row['tests_to_keep'] = $entity->get('tests_to_keep') . ' day(s)';
    }
    else {
      $row['tests_to_keep'] = $entity->get('tests_to_keep') . ' day';
    }

    $row['sync_daily'] = $entity->get('sync_daily') ? 'Yes' : 'No';
    $row['email_enabled'] = $entity->get('email_enabled') ? 'Yes' : 'No';
    $row['test_runs'] = count($nids);
    $row['last_date_synced'] = $entity->get('last_date_synced') ?? 'Not Synced.';
    if ($entity->get('last_date_synced')) {
      $now = new DateTime(date('Y-m-d'));
      $last_date = new DateTime(date("Y-m-d", strtotime($entity->get('last_date_synced'))));
      if (2 <= $now->diff($last_date)->days) {
        $row['last_date_synced'] = ['class' => "api-status-red", 'data' => $entity->get('last_date_synced')];
      }
      else {
        $row['last_date_synced'] = $entity->get('last_date_synced');
      }
    }
    else {
      $row['last_date_synced'] = $this->t('Please Sync data.');
    }

    $test_statuses = '';
    if ( $entity->get('count_failing') > 0 ) {
      $test_statuses .= '<span class="status status-failing"><i class="fa fa-times-circle"></i> ' . $entity->get('count_failing') . ' ' . $this->t('Failed') . '</span>';
    }
    if ( $entity->get('count_passing') > 0 ) {
      $test_statuses .= '<span class="status status-passing"><i class="fa fa-check-circle"></i> ' . $entity->get('count_passing') . ' ' . $this->t('Passing') . '</span>';
    }
    if ( $entity->get('count_failing') == 0 && $entity->get('count_passing') == 0 ) {
      $test_statuses = $this->t('Please Sync data.');
    }
    $row['test_statuses'] = Markup::create($test_statuses);

    if ($entity->status()) {
      $row['status'] = ['class' => 'api-status-green', 'data' => 'ACTIVE'];
    }
    else {
      $row['status'] = ['class' => 'api-status-red', 'data' => 'INACTIVE'];
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
    $operations = parent::getDefaultOperations($entity);

    if (!$entity->get('status') && $entity->hasLinkTemplate('enable') && strpos($entity->get('name'), "API Call Failed") === FALSE) {
      $operations['enable'] = [
        'title' => $this->t('Enable'),
        'weight' => 40,
        'url' => $entity->toUrl('enable'),
      ];
    }
    elseif ($entity->hasLinkTemplate('disable') && strpos($entity->get('name'), "API Call Failed") === FALSE) {
      $operations['disable'] = [
        'title' => $this->t('Disable'),
        'weight' => 50,
        'url' => $entity->toUrl('disable'),
      ];
      $operations['sync'] = [
        'title' => $this->t('Sync'),
        'weight' => 50,
        'url' => $entity->toUrl('sync'),
      ];
    }
    return $operations;
  }

}
