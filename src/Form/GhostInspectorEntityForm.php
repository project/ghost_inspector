<?php

namespace Drupal\ghost_inspector\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The entity form.
 */
class GhostInspectorEntityForm extends EntityForm {

  /**
   * The drupal state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\Core\Extension\ThemeHandler definition.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The drupal state.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler.
   */
  public function __construct(StateInterface $state, ThemeHandlerInterface $themeHandler) {
    $this->state = $state;
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\ghost_inspector\Entity\GhostInspectorEntityInterface $ghost_inspector */
    $ghost_inspector = $this->entity;

    // Basics API setup.
    $form['ghost_inspector_create'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API settings'),
    ];

    $form['ghost_inspector_create']['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#description' => $this->t('Active Suite get used by default.'),
      '#required' => TRUE,
      '#options' => [FALSE => $this->t('INACTIVE'), TRUE => $this->t('ACTIVE')],
      '#default_value' => $ghost_inspector->get('status'),
    ];

    $form['ghost_inspector_create']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t("API Key"),
      '#size' => 40,
      '#required' => TRUE,
      '#default_value' => $ghost_inspector->get('api_key'),
      '#description' => $this->t('Your API key provided in your account.'),
    ];

    $form['ghost_inspector_create']['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Suite Id"),
      '#size' => 25,
      '#required' => TRUE,
      '#default_value' => $ghost_inspector->id(),
      '#description' => $this->t('The ID of the suite to fetch.'),
    ];

    // PURGE settings.
    $form['ghost_inspector_create_purge'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Purge old test Runs'),
    ];

    $tests_to_keep = $ghost_inspector->get('tests_to_keep');
    if (empty($tests_to_keep) || $tests_to_keep === "") {
      $tests_to_keep = 7;
    }
    $form['ghost_inspector_create_purge']['tests_to_keep'] = [
      '#type' => 'textfield',
      '#attributes' => [
        ' type' => 'number',
      ],
      '#min' => 0,
      '#max' => 999,
      '#title' => 'Keep test runs for # day(s). ',
      '#required' => TRUE,
      '#default_value' => $tests_to_keep,
      '#maxlength' => 3,
      '#description' => $this->t('Max number of days is 999.'),
    ];

    // Cron JOB Error Notification Email.
    $form['ghost_inspector_create_email'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cron JOB Error Notification Email'),
    ];

    $form['ghost_inspector_create_email']['sync_daily'] = [
      '#type' => 'select',
      '#title' => $this->t('Sync daily'),
      '#required' => TRUE,
      '#description' => $this->t('Enabled suite CRON JOB to run daily.'),
      '#options' => [FALSE => $this->t('No'), TRUE => $this->t('Yes')],
      '#default_value' => $ghost_inspector->get('sync_daily'),
    ];

    $form['ghost_inspector_create_email']['email_enabled'] = [
      '#type' => 'select',
      '#title' => $this->t('Enable Error Notification'),
      '#description' => $this->t('Configure whether to receive a notification upon there being a cronjob error.'),
      '#options' => [FALSE => $this->t('No'), TRUE => $this->t('Yes')],
      '#default_value' => $ghost_inspector->get('email_enabled'),
    ];

    $form['ghost_inspector_create_email']['email_addresses'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email Addresses'),
      '#description' => $this->t('A list of email addresses to be notified of a cronjob error. (One email address per line)'),
      '#default_value' => $ghost_inspector->get('email_addresses'),
    ];

    $email_subject_line = $ghost_inspector->get('email_subject_line');
    if (empty($email_subject_line) || $email_subject_line === "") {
      $email_subject_line = "Ghost Inspector Test Run Failed Notification";
    }
    $form['ghost_inspector_create_email']['email_subject_line'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Subject Line'),
      '#description' => $this->t('The subject line of the notification email.'),
      '#default_value' => $email_subject_line,
    ];

    $email_template = $ghost_inspector->get('email_template');
    if (empty($email_template) || $email_template === "") {
      $email_template = "Hi.\r\n\r\nGhost Inspector Test failed on [site-name] for Suite Name: [suite-name]. Click here to view test run [test-run-url].\r\n\r\n[test-results-table]\r\n\r\nKind Regards.";
    }
    $form['ghost_inspector_create_email']['email_template'] = [
      '#title' => $this->t('Email Template'),
      '#type' => 'textarea',
      '#default_value' => $email_template,
      '#description' => $this->t('Provide a template of failed email notification. <br/>Replacements values available:  [suite-id], [suite-name], [site-name], [test-run-url], [test-results-table].'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $ghost_inspector = $this->entity;
    $status = $ghost_inspector->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the Ghost Inspector Suite %id settings.', [
          '%id' => $ghost_inspector->id(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the Ghost Inspector Suite %id settings.', [
          '%id' => $ghost_inspector->id(),
        ]));
    }

    // Test API call and update fields.
    getSuite($this->entity);

    $form_state->setRedirectUrl($ghost_inspector->toUrl('collection'));
  }

}
