<?php

namespace Drupal\ghost_inspector\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Ghost Inspector setting entity.
 *
 * @ConfigEntityType(
 *   id = "ghost_inspector",
 *   label = @Translation("Ghost Inspector"),
 *   handlers = {
 *     "view_builder" = "Drupal\ghost_inspector\GhostInspectorEntityViewBuilder",
 *     "list_builder" = "Drupal\ghost_inspector\GhostInspectorEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ghost_inspector\Form\GhostInspectorEntityForm",
 *       "edit" = "Drupal\ghost_inspector\Form\GhostInspectorEntityForm",
 *       "delete" = "Drupal\ghost_inspector\Form\GhostInspectorEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\ghost_inspector\GhostInspectorEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "ghost_inspector",
 *   admin_permission = "administer ghost inspector",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/ghost-inspector/{ghost_inspector}",
 *     "add-form" = "/admin/config/system/ghost-inspector/add",
 *     "edit-form" = "/admin/config/system/ghost-inspector/{ghost_inspector}/edit",
 *     "delete-form" = "/admin/config/system/ghost-inspector/{ghost_inspector}/delete",
 *     "enable" = "/admin/config/system/ghost-inspector/{ghost_inspector}/enable",
 *     "disable" = "/admin/config/system/ghost-inspector/{ghost_inspector}/disable",
 *     "sync" = "/admin/config/system/ghost-inspector/{ghost_inspector}/sync",
 *     "execute-suite" = "/admin/config/system/ghost-inspector/{ghost_inspector}/execute-suite",
 *     "dashboardreport" = "/admin/config/system/ghost-inspector/dashboard",
 *     "collection" = "/admin/config/system/ghost-inspector"
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "status",
 *     "sync_daily",
 *     "api_key",
 *     "tests_to_keep",
 *     "last_date_synced",
 *     "count_failing",
 *     "count_passing",
 *     "email_enabled",
 *     "email_addresses",
 *     "email_subject_line",
 *     "email_template",
 *   }
 * )
 */
class GhostInspectorEntity extends ConfigEntityBase implements GhostInspectorEntityInterface {

  /**
   * The Ghost Inspector setting suite id.
   *
   * @var string
   */
  protected $id;

  /**
   * The Ghost Inspector setting suite name.
   *
   * @var string
   */
  protected $name;

  /**
   * Include the status.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * Include the sync daily.
   *
   * @var bool
   */
  protected $sync_daily = TRUE;


  /**
   * The Ghost Inspector setting api key.
   *
   * @var string
   */
  protected $api_key;

  /**
   * The Ghost Inspector setting tests to keep.
   *
   * @var integer
   */
  protected $tests_to_keep = 0;

  /**
   * The Ghost Inspector setting last date synced.
   *
   * @var integer
   */
  protected $last_date_synced = 0;

  /**
   * The Ghost Inspector setting last count failing.
   *
   * @var integer
   */
  protected $count_failing = 0;

  /**
   * The Ghost Inspector setting last count passing.
   *
   * @var integer
   */
  protected $count_passing = 0;

  /**
   * The Ghost Inspector setting email enabled.
   *
   * @var bool
   */
  protected $email_enabled = FALSE;

  /**
   * The Ghost Inspector setting email addresses.
   *
   * @var string
   */
  protected $email_addresses = '';

  /**
   * The Ghost Inspector setting email subject line.
   *
   * @var string
   */
  protected $email_subject_line = '';

  /**
   * The Ghost Inspector setting email template.
   *
   * @var string
   */
  protected $email_template = '';

}
