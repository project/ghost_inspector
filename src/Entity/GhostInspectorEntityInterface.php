<?php

namespace Drupal\ghost_inspector\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Configuration Split setting entities.
 */
interface GhostInspectorEntityInterface extends ConfigEntityInterface {

}
