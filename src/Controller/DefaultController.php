<?php

namespace Drupal\ghost_inspector\Controller;

use Drupal\Core\Controller\ControllerBase;


/**
 * The controller for Ghost Inspector actions.
 */
class DefaultController extends ControllerBase
{

  /**
   * Enable the Suite.
   *
   * @param string $ghost_inspector
   *   The ghost inspector name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function enableEntity($ghost_inspector) {
    $entity = $this->entityTypeManager()->getStorage('ghost_inspector')->load($ghost_inspector);
    $entity->set('status', TRUE);
    $entity->save();

    return $this->redirect('entity.ghost_inspector.collection');
  }

  /**
   * Disable the Suite.
   *
   * @param string $ghost_inspector
   *   The ghost inspector name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function disableEntity($ghost_inspector) {
    $entity = $this->entityTypeManager()->getStorage('ghost_inspector')->load($ghost_inspector);
    $entity->set('status', FALSE);
    $entity->save();

    // Purge Test Runs.
    purgeTestRuns($entity);

    return $this->redirect('entity.ghost_inspector.collection');
  }

  /**
   * Sync the Suite.
   *
   * @param string $ghost_inspector
   *   The ghost inspector name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function syncEntity($ghost_inspector) {
    $entity = $this->entityTypeManager()->getStorage('ghost_inspector')->load($ghost_inspector);

    // Sync with API.
    getListResultsOnSuite($entity);

    return $this->redirect('ghost_inspector.dashboard', ['ghost_inspector' => $ghost_inspector]);
  }

  /**
   * Execute Suite.
   *
   * @param string $ghost_inspector
   *   The ghost inspector name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function executeSuiteEntity($ghost_inspector) {
    $entity = $this->entityTypeManager()->getStorage('ghost_inspector')->load($ghost_inspector);

    // Execute Suite with API.
    executeSuite($entity);

    return $this->redirect('ghost_inspector.dashboard', ['ghost_inspector' => $ghost_inspector]);
  }

}
