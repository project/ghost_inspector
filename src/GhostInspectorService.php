<?php

namespace Drupal\ghost_inspector;

use GuzzleHttp\Client;

/**
 * Class GhostInspectorService.
 */
class GhostInspectorService {

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new GhostInspectorService object.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ghost_inspector.service')
    );
  }

}
